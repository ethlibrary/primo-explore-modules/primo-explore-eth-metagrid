export class ethMetagridController {

    constructor( ethConfigService, ethMetagridConfig, ethMetagridService ) {
        this.ethConfigService = ethConfigService;
        this.config = ethMetagridConfig;
        this.ethMetagridService = ethMetagridService;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;

            // this.persons = collection for all persons having a GND ID
            this.persons = [];

            if((!this.parentCtrl.item.pnx.display.lds03 || this.parentCtrl.item.pnx.display.lds03.length === 0) && (!this.parentCtrl.item.pnx.display.lds90 || this.parentCtrl.item.pnx.display.lds90.length === 0))return;

            // array of all GND IDs
            let gndIds = [];

            // array of all IdRef IDs            
            let idRefs = [];

            if(this.parentCtrl.item.pnx.display.lds03 && this.parentCtrl.item.pnx.display.lds03.length > 0){
                // GND ID in local display field 03
                let lds03 = this.parentCtrl.item.pnx.display.lds03;
                for(let i = 0; i < lds03.length; i++){
                    // ALMA Ressources: link in value
                    if(lds03[i].indexOf('/gnd/') > -1){
                        let part = lds03[i].substring(lds03[i].indexOf('/gnd/') + 5);
                        part = part.substring(0, part.indexOf('">'));
                        if(part.indexOf('(DE-588)')>-1){
                            part = part.replace('(DE-588)','')
                        }
                        if(gndIds.indexOf(part)===-1)gndIds.push(part);
                    }
                    // External data: text in value
                    else if (lds03[i].indexOf(': ') > -1) {
                        let part = lds03[i].substring(lds03[i].indexOf(': ') + 2);
                        if(part.indexOf('(DE-588)')>-1){
                            part = part.replace('(DE-588)','')
                        }
                        if(gndIds.indexOf(part)===-1)gndIds.push(part);
                    }
                }
            }

            for(var i = 0; i < gndIds.length; i++){
                let gndId = gndIds[i];
                if (gndId === "")continue;
                if (gndId.indexOf(')')>-1) {
                    gndId = gndId.substring(gndId.lastIndexOf(')')+1);
                }
                this.ethMetagridService.getConcordances('gnd', gndId)
                    .then((data) => {
                        this.processMetagridResponse(data);
                    })
            }


            if(this.parentCtrl.item.pnx.display.lds90 && this.parentCtrl.item.pnx.display.lds90.length > 0){
                // <a target="_blank" href="https://www.idref.fr/026875659"> Frisch, Max 1911-1991</a>
                let lds90 = this.parentCtrl.item.pnx.display.lds90;
                for(let i = 0; i < lds90.length; i++){
                    let part = lds90[i].substring(lds90[i].indexOf('idref.fr/') + 9);
                    part = part.substring(0, part.indexOf('">'));
                    if(idRefs.indexOf(part)===-1)idRefs.push(part);
                }
            }
            for(var i = 0; i < idRefs.length; i++){
                let id = idRefs[i];
                if (id === "")continue;
                this.ethMetagridService.getConcordances('sudoc', id)
                    .then((data) => {
                        this.processMetagridResponse(data);
                    })
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethMetagridController.onInit()\n\n");
            console.error(e.message);
            throw(e);
        }
    }

    processMetagridResponse(data){
        try{
            if (!data)return;
            if (!data.concordances || data.concordances.length === 0 || !data.concordances[0].resources || data.concordances[0].resources.length === 0)return;
            let person = {};
            let resources = data.concordances[0].resources;
            let whitelistedMetagridLinks = [];
            let whitelistedMetagridLinksSorted = [];

            let personId = data.concordances[0].id;
            let isPersonDone = this.persons.some( p => {
                return p.id == personId;
            })
            if(isPersonDone)return;
            person.id = data.concordances[0].id;
            person.name = resources[0].metadata.last_name;
            if(resources[0].metadata.first_name){
                person.name += ', ' + resources[0].metadata.first_name;
            }

            for(var j = 0; j < resources.length; j++){
                let resource = resources[j];
                let slug = resource.provider.slug;
                let url = resource.link.uri;

                // check whitelist for Metagrid links
                if (this.config.whitelist.indexOf(slug) === -1) {
                    continue;
                }
                let label = this.ethConfigService.getLabel(this.config, slug);
                whitelistedMetagridLinks.push({'slug': slug,'url': url, 'label': label});
            }
            // Dodis and HLS first
            let dodis = whitelistedMetagridLinks.filter(e => {
                return e.slug === 'dodis';
            });
            whitelistedMetagridLinksSorted = whitelistedMetagridLinksSorted.concat(dodis);
            let hls = whitelistedMetagridLinks.filter(e => {
                return e.slug === 'hls-dhs-dss';
            });
            whitelistedMetagridLinksSorted = whitelistedMetagridLinksSorted.concat(hls);
            let rest = whitelistedMetagridLinks.filter(e => {
                return e.slug !== 'hls-dhs-dss' && e.slug !== 'dodis';
            });
            whitelistedMetagridLinksSorted = whitelistedMetagridLinksSorted.concat(rest);

            person.metagridLinks = whitelistedMetagridLinksSorted;
            this.persons.push(person);
        }
        catch(e){
            console.error("an error occured: ethMetagridController: ethMetagridService.processMetagridResponse:\n\n");
            console.error(e.message);
        }
    }
}

ethMetagridController.$inject = ['ethConfigService', 'ethMetagridConfig', 'ethMetagridService'];
