export const ethMetagridService = ['$http', '$sce', function($http, $sce){
 

    function getConcordances(provider, id ){
        let url = 'https://api.metagrid.ch/search?group=1&skip=0&take=50&provider=' + provider + '&query=' + id;
        $sce.trustAsResourceUrl(url);

        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "an error occured: ethMetagridService callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            );
    }
    return {
        getConcordances: getConcordances
    };
}]
